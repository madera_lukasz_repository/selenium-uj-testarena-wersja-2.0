import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class LoginTest extends BaseTest{

    @Test
    public void shouldIdLogin(){
        loginPage.openTestArena();
        loginPage.fillEmail("administrator@testarena.pl");
        loginPage.fillPassword("sumXQQ72$L");
        loginPage.clickLoginButton();

        Assert.assertTrue(dashboardPage.isLogoutButtonPresent());
        //Assert.assertTrue(dashboardPage.isLogoutButtonVisible());
    }

    @Test
    public void niePowinienZalogowacZBlednymHaslemOrazZwrocicBlad() {
        loginPage.openTestArena();
        loginPage.fillEmail("administrator@testarena.pl");
        loginPage.fillPassword("admin");
        loginPage.clickLoginButton();

        Assert.assertTrue(dashboardPage.loginErrorPresent());
    }

    @Test
    public void niePowinienZalogowacZBlednymMailemOrazZwrocicBlad() {
        loginPage.openTestArena();
        loginPage.fillEmail("adminisfstrator@tedfsstarena.pl");
        loginPage.fillPassword("sumXQQ72$L");
        loginPage.clickLoginButton();

        Assert.assertTrue(dashboardPage.loginErrorPresent());
    }

    @Test
    public void shouldNotLoginWithoutLoginAndPasswordShouldAppearErrorMessage() {
        loginPage.openTestArena();
        loginPage.clickLoginButton();
        Assert.assertTrue(dashboardPage.loginErrorPresent());
    }

}
