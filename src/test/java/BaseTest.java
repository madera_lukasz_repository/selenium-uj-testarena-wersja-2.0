import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.BeforeTest;

import java.sql.Time;
import java.util.concurrent.TimeUnit;

public class BaseTest {

    WebDriver driver; //w zwiazku z utworzeniem beforetest nie mozemy inicjalizowac driver przez podaniem sciezki do niego
    LoginPage loginPage; //deklarujemy  xeby isntiało do wszystkich pol
    DashboardPage dashboardPage;
    ProjectPage projectPage;

    @BeforeTest //tylko te ktore są oznaczone jako test //method przed kazda metoda w klasie
    public void before() {
        //System.setProperty("webdriver.gecko.driver","C:\\selendriver\\geckodriver.exe");
        System.setProperty("webdriver.chrome.driver","C:\\selendriver\\chromedriver.exe");
        //driver = new FirefoxDriver(); //inicjalizacja drivera firefox
        driver = new ChromeDriver(); //inicjalizacja drivera chrome
        driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
        //driver = new ChromeDriver();
        loginPage = new LoginPage(driver);
        dashboardPage = new DashboardPage(driver);
        projectPage = new ProjectPage(driver); //zanim tu dodam to u gory musze zainicjalizowac     ProjectPage projectPage;

    }
}
