import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.util.Date;
import java.util.Random;

public class ProjectTest extends BaseTest {

    @BeforeTest
    public void prepare() {
        loginPage.openTestArena();
        loginPage.fillEmail("administrator@testarena.pl");
        loginPage.fillPassword("sumXQQ72$L");
        loginPage.clickLoginButton();

    }

    @Test
    //utworzenie projektu
    public void addProject() {
        projectPage.clickProjectButton();
        projectPage.clickManageButton();
        projectPage.clickProject2Button();
        projectPage.clickAddButton();
        projectPage.fillName("dodanie_lmadera" + new Date().getTime());
        projectPage.fillPrefix("LM07" + new Helpers().getHelpers());
        projectPage.fillDescription("automat dokonuje utworzenia nowego projektu" + new Date().getTime());
        projectPage.clickSaveProject();

        new WebDriverWait(driver, 10)
                .until(ExpectedConditions.textToBePresentInElementLocated(By.cssSelector("#j_info_box p"), "Project successfully added.")); // ewentualnie elementToBeClickable
        //sprawdzenie czy sie udalo dodac
        //sprawdzneie EN, w przypadku wybranego jezyka PL nie zadziala
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    @Test
    //sprawdzam czy w przypadku braku uzupełnionego formularz uda się dodać projekt
    public void addProjectWithEmptyData() {
        projectPage.clickProjectButton();
        projectPage.clickManageButton();
        projectPage.clickProject2Button();
        projectPage.clickAddButton();
        projectPage.clickSaveProject();
        Assert.assertTrue(dashboardPage.validErrorPresent());

    }

    @Test
    //sprawdzam czy w przypadku braku uzupełnionego prefixu uda się dodać projekt
    public void addProjectWithEmptyPrefix() {
        projectPage.clickProjectButton();
        projectPage.clickManageButton();
        projectPage.clickProject2Button();
        projectPage.clickAddButton();
        projectPage.fillName("dodanie_lmadera" + new Date().getTime());
        projectPage.clickSaveProject();

        Assert.assertTrue(dashboardPage.validErrorPresent());

    }

    @Test
    //sprawdzam czy w przypadku braku uzupełnionej nazwy uda się dodać projekt
    public void addProjectWithEmptyName() {
        projectPage.clickProjectButton();
        projectPage.clickManageButton();
        projectPage.clickProject2Button();
        projectPage.clickAddButton();
        projectPage.fillPrefix("LM05" + new Helpers().getHelpers());
        projectPage.clickSaveProject();

        Assert.assertTrue(dashboardPage.validErrorPresent());

    }

    @Test
    public void editProject() {
        //edycja projektu oraz próba zapisu
        projectPage.clickProjectButton();
        projectPage.clickProjectButton();
        projectPage.clickManageButton();
        projectPage.clickProject2Button();
        projectPage.clickElementEdit();
        projectPage.clickEditButton();
        projectPage.fillName("edycja_lmadera" + new Date().getTime());
        projectPage.fillPrefix("LM18" + new Helpers().getHelpers());
        projectPage.fillDescription("automat dokonuje edycji projektu"+ new Date().getTime());
        projectPage.clickSaveProject();

        new WebDriverWait(driver, 10)
                .until(ExpectedConditions.textToBePresentInElementLocated(By.cssSelector("#j_info_box p"), "Changes saved.")); // ewentualnie elementToBeClickable
        //sprawdzenie czy sie udalo dodac
        //sprawdzneie EN, w przypadku wybranego jezyka PL nie zadziala
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Test
    //edycja projektu z usunięciem prefixu oraz proba zapisu
    public void editProjectWithEmptyPrefix() {
        projectPage.clickProjectButton();
        projectPage.clickProjectButton();
        projectPage.clickManageButton();
        projectPage.clickProject2Button();
        projectPage.clickElementEdit();
        projectPage.clickEditButton();
        driver.findElement(By.cssSelector("#prefix")).clear();
        projectPage.fillDescription("automat dokonuje edycji projektu"+ new Date().getTime());
        projectPage.clickSaveProject();

        Assert.assertTrue(dashboardPage.validErrorPresent());

    }

    @Test
    //edycja projektu z usunięciem nazwy oraz proba zapisu
    public void editProjectWithEmptyName() {
        projectPage.clickProjectButton();
        projectPage.clickProjectButton();
        projectPage.clickManageButton();
        projectPage.clickProject2Button();
        projectPage.clickElementEdit();
        projectPage.clickEditButton();
        //driver.findElement(By.cssSelector("#name")).clear();
        projectPage.clearPrefix();
        projectPage.fillDescription("automat dokonuje edycji projektu"+ new Date().getTime());
        projectPage.clickSaveProject();

        Assert.assertTrue(dashboardPage.validErrorPresent());

    }

}
