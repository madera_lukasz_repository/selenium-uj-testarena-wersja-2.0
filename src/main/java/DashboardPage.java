import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class DashboardPage extends BasePage {

    //bedzie nam obslugiwać stronę głowna

    @FindBy(css = ".header_logout a")
    WebElement logOutButton;

//    WebDriver driver; --wywalam bo mam w basepage

    public DashboardPage(WebDriver driver) {
        super(driver);
        //       this.driver = driver; --wywalam
    }


    public boolean isLogoutButtonVisible() {
        return logOutButton.isDisplayed();
    }

    public boolean isLogoutButtonPresent() {
        List<WebElement> tmp = driver.findElements(By.cssSelector(".header_logout a"));
        return tmp.size() != 0;
    }
    //druga taka sama metoda

//    public boolean isLogoutButtonVisible() {
//        //return driver.findElement(By.cssSelector(".header_loguot a")).isDisplayed();//sprawdzamy czy jest widoczny
//        //to samo rozpisane
//        WebElement tmp = driver.findElement(By.cssSelector(".header_loguout a"));
//        return tmp.isDisplayed();
//    }

    //metoda blednego logowania
    public boolean loginErrorPresent() {
        List<WebElement> tmp = driver.findElements(By.cssSelector(".login_form_error"));
        //System.out.println(tmp.size());
        return tmp.size() > 0;
    }

    //metoda validacji błędu
    public boolean validErrorPresent() {
        List<WebElement> tmp = driver.findElements(By.cssSelector(".error_msg"));
        System.out.println(tmp.size());
        return tmp.size() !=0;
    }

}
