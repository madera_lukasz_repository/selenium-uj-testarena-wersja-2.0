import jdk.nashorn.internal.objects.annotations.Function;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class ProjectPage extends BasePage {


    @FindBy(className = "item3")
    WebElement clickProject;

    @FindBy(xpath = "/html/body/div[1]/section/article/nav/ul/li[1]/a")
    WebElement clickManage;

    @FindBy(xpath = "/html/body/aside/div/ul/li[1]/a")
    WebElement clickProject2;

    @FindBy(xpath = "/html/body/div[1]/section/article/nav/ul/li[1]/a")
    WebElement clickAdd;

    @FindBy(css = "#name")
    WebElement nameInput;

    @FindBy(css = "#prefix")
    WebElement prefixInput;

    @FindBy(css = "#description")
    WebElement descriptionInput;

    @FindBy(css = "#save")
    WebElement saveInput;

    @FindBy(css = "#content > article > nav > ul > li:nth-child(1) > a")
    WebElement clickEdit;

    @FindBy(css = "#content > article > table > tbody > tr:nth-child(5) > td:nth-child(1) > a")
    WebElement clickEditElement;

    @FindBy (className = "#error_msg")
    WebElement validName;


    public ProjectPage(WebDriver driver) {
        super(driver);
    }

    public void clickProjectButton() {
        clickProject.click();
    }

    public void clickManageButton() {
        clickManage.click();
    }

    public void clickProject2Button() {
        clickProject2.click();
    }

    public void clickAddButton() {
        clickAdd.click();
    }

    public void fillName(String name) {
        nameInput.sendKeys(name);
    }

    public void fillPrefix(String prefix) {prefixInput.sendKeys(prefix);    }

    public void fillDescription(String description) {
        descriptionInput.sendKeys(description);
    }

    public void clickSaveProject() {
        saveInput.click();
    }

    public void clickEditButton() { clickEdit.click(); }

    public void clickElementEdit() { clickEditElement.click(); }

    public void clearPrefix() { prefixInput.clear(); }



}
